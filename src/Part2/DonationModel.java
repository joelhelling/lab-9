/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Part2;

import java.util.*;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

import com.sun.mail.smtp.SMTPTransport;

/**
 *
 * @author joel.helling904
 */
public class DonationModel {
    private double donationAmount;
    
    public DonationModel()
    {
        this.donationAmount = 0.0;
    }
    public DonationModel(double inputAmount)
    {
        this.donationAmount = inputAmount;
    }
    
    public void setDonationAmount(double inputDonation)
    {
        this.donationAmount = inputDonation;
    }
    
    public double getDonationAmount()
    {
        return this.donationAmount;
    }
    
    public String calculatePlague(int donationAmount)
    {
        if(donationAmount < 5)
        {
            return "NO PRIZE";
        }
        else if(donationAmount >= 5 && donationAmount < 501)
        {
            return "DEAN EMAIL";
        }
        else if(donationAmount >= 501 && donationAmount < 1001)
        {
            return "BRONZE";
        }
        else if(donationAmount >= 1001 && donationAmount < 3000)
        {
            return "SILVER";
        }
        else
        {
            return "GOLD";
        }
    }
    
    public boolean sendEmail(String name, String email, String prize)
    {
        String from = "deanofplagues@plaguesinc.edu";
        String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        String USER_NAME = "stoof67";
        String PASSWORD = "1243568790";
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtps.host", "smtp.gmail.com");
        properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.port", "465");
        properties.setProperty("mail.smtp.socketFactory.port", "465");
        properties.setProperty("mail.smtps.auth", "true");
        properties.put("mail.smtps.quitwait", "false");
        
        String htmlStuff = ""
                + "<p3>Dear " + name + ",</p3><br><br>"
                + "<p4>Thank you for your donation to "
                + "Cama State University. Your interest and contribution are  "
                + "greatly appreciated.</p4><br><br><br>"
                + "<img src=\"cid:plague-img-1\"/><br>"
                + "<img src=\"cid:sign-img-1\"/><br><br><br><br>"
                + "<p1>Sincerely,<br>Peter Miller</p1><br>"
                + "<p2>Administrator of Fundrasing, Cama State University</p><br>";
        
        Session session = Session.getInstance(properties, null);
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Donation Confirmation");
            Multipart multipart = new MimeMultipart();
            BodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(htmlStuff, "text/html");
            multipart.addBodyPart(htmlPart);
            
            String plagueType = null;
            switch (prize) {
                case "BRONZE":
                    plagueType = "src/resource/bronze.jpg";
                    break;
                case "SILVER":
                    plagueType = "src/resource/silver.jpg";
                    break;
                case "GOLD":
                    plagueType = "src/resource/gold.jpg";
                    break;
            }
            
            BodyPart plaguePart = new MimeBodyPart();
            DataSource ds = new FileDataSource(plagueType);
            plaguePart.setDataHandler(new DataHandler(ds));
            plaguePart.setHeader("Content-ID", "<plague-img-1>");
            multipart.addBodyPart(plaguePart);
            
            BodyPart signPart = new MimeBodyPart();
            ds = new FileDataSource("src/resource/Signature.jpeg");
            signPart.setDataHandler(new DataHandler(ds));
            signPart.setHeader("Content-ID", "<sign-img-1>");
            multipart.addBodyPart(signPart);
             
            message.setContent(multipart);
            
            SMTPTransport t = (SMTPTransport) session.getTransport("smtps");
            t.connect("smtp.gmail.com", USER_NAME, PASSWORD);
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            System.out.println("Success");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
        return false;
    }
}
