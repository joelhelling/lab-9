/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Part2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author joel.helling904
 */
public class DonationController {

    private DonationModel model;
    private DonationView view;

    public DonationController(DonationView view, DonationModel model) {
        this.view = view;
        this.model = model;

        view.addListenerSubmit(new ListenerSubmit());
        view.addListenerCancel(new CancelSubmit());
    }

    private class CancelSubmit implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                view.setVisible(false);
                view.dispose();
            } catch (NumberFormatException nfex) {
                //view.showError("Bad input: '" + amount + "'");
            }
        }
    }

    private class ListenerSubmit implements ActionListener {
         
        //private int numberOfDonors = 0;
        public void actionPerformed(ActionEvent e) {
            try {
                //this.numberOfDonors++;
                String name = view.getName();
                String email = view.getEmail();
                int donation = view.getDonation();
                String s[] = name.split(" ");
                try {
                    String host = "jdbc:derby://localhost:1527/Cama State Donations";
                    String dName = "student";
                    String dPass = "student";
                    Connection conn = DriverManager.getConnection(host, dName, dPass);
                    Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    String SQL = "INSERT INTO STUDENT.Donations (LASTNAME,FIRSTNAME,EMAIL,DONATIONAMOUNT)" 
                            + " VALUES ('" + s[1] + "','" + s[0] + "','" + email + "',"+ (double) donation + ")";
                   state.executeUpdate(SQL);
                } catch (SQLException err) {
                    System.out.println(err.getMessage());
                }

                //System.out.println(model.calculatePlague(donation));
                view.setVisible(false);
                view.dispose();
                //boolean sentEmail = model.sendEmail(name, email,model.calculatePlague(donation));
                //System.out.printf("%s\n", sentEmail);
            } catch (NumberFormatException nfex) {
                //view.showError("Bad input: '" + amount + "'");
            }
        }
    }
}
