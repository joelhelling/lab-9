/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Part2;

/**
 *
 * @author kelsey.pritsker676
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class DonationManager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DonationModel model = new DonationModel();
        DonationView view = new DonationView();
        Scanner scan = new Scanner(System.in);

        DonationController controller = new DonationController(view, model);

        try {
            String host = "jdbc:derby://localhost:1527/Cama State Donations";
            String dName = "student";
            String dPass = "student";
            Connection conn = DriverManager.getConnection(host, dName, dPass);
            Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            String SQL;
            ResultSet Rset;
            do {
                System.out.print("Enter Command: ");
                String line = scan.nextLine();
                String s[];
                s = line.split(" ");

                switch (s[0].toLowerCase()) {
                    case "donate":
                        view.setVisible(true);
                        break;
                    case "list":
                        SQL = "SELECT * FROM STUDENT.Donations";
                        Rset = state.executeQuery(SQL);

                        while (Rset.next()) {;
                            String lName = Rset.getString("LASTNAME");
                            String fName = Rset.getString("FIRSTNAME");
                            String email = Rset.getString("EMAIL");
                            double donationAmount = Rset.getDouble("DONATIONAMOUNT");

                           System.out.println(lName + ",   " + fName + "    " + email + " " + ": " + donationAmount + "  ");
                        }
                        break;
                    case "bronze":
                        SQL = "SELECT * FROM STUDENT.Donations WHERE DONATIONAMOUNT BETWEEN 500 AND 1000";
                        Rset = state.executeQuery(SQL);

                       while (Rset.next()) {;
                            String lName = Rset.getString("LASTNAME");
                            String fName = Rset.getString("FIRSTNAME");
                            String email = Rset.getString("EMAIL");
                            double donationAmount = Rset.getDouble("DONATIONAMOUNT");

                           System.out.println(lName + ",   " + fName + "    " + email + " " + ": " + donationAmount + "  ");
                        }
                        break;
                    case "silver":
                        SQL = "SELECT * FROM STUDENT.Donations WHERE DONATIONAMOUNT BETWEEN 1001 AND 3000";
                        Rset = state.executeQuery(SQL);

                        while (Rset.next()) {;
                            String lName = Rset.getString("LASTNAME");
                            String fName = Rset.getString("FIRSTNAME");
                            String email = Rset.getString("EMAIL");
                            double donationAmount = Rset.getDouble("DONATIONAMOUNT");

                            System.out.println(lName + ",   " + fName + "    " + email + " " + ": " + donationAmount + "  ");
                        }
                        break;
                    case "gold":
                        SQL = "SELECT * FROM STUDENT.Donations WHERE DONATIONAMOUNT BETWEEN 3001 AND 5000";
                        Rset = state.executeQuery(SQL);

                        while (Rset.next()) {;
                            String lName = Rset.getString("LASTNAME");
                            String fName = Rset.getString("FIRSTNAME");
                            String email = Rset.getString("EMAIL");
                            double donationAmount = Rset.getDouble("DONATIONAMOUNT");

                            System.out.println(lName + ",   " + fName + "    " + email + " " + ": " + donationAmount + "  ");
                        }
                        break;
                    case "last":
                        
                        SQL = "SELECT * FROM STUDENT.Donations WHERE LastName='" + s[1] + "'";
                        Rset = state.executeQuery(SQL);

                        while (Rset.next()) {;
                            String lName = Rset.getString("LASTNAME");
                            String fName = Rset.getString("FIRSTNAME");
                            String email = Rset.getString("EMAIL");
                            double donationAmount = Rset.getDouble("DONATIONAMOUNT");

                            System.out.println(lName + ",   " + fName + "    " + email + " " + ": " + donationAmount + "  ");
                        }
                        break;
                        case "first":
                        
                        SQL = "SELECT * FROM STUDENT.Donations WHERE FirstName='" + s[1] + "'";
                        Rset = state.executeQuery(SQL);

                        while (Rset.next()) {;
                            String lName = Rset.getString("LASTNAME");
                            String fName = Rset.getString("FIRSTNAME");
                            String email = Rset.getString("EMAIL");
                            double donationAmount = Rset.getDouble("DONATIONAMOUNT");

                            System.out.println(lName + ",   " + fName + "    " + email + " " + ": " + donationAmount + "  ");
                        }
                        break;
                    case "quit":
                        System.out.println("Have a nice day");
                        System.exit(0);
                    default:
                        break;
                }

            } while (true);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }
}
