/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


public class DB_connect {
 
    public static void main(String[] args) {
    try{
        String host = "jdbc:derby://localhost:1527/Contacts";
        String dName = "student";
        String dPass= "student";
        Connection conn = DriverManager.getConnection( host, dName, dPass );
        Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        String SQL = "SELECT * FROM STUDENT.Colleagues";
        ResultSet Rset = state.executeQuery( SQL );
       
        System.out.println("ID"+"   "+"FIRSTNAME"+"  "+"LASTNAME"+"   "+"TITLE"+"    "+"DEPARTMENT"+"  "+"ADDRESS"+"   "+"EMAIL");
        while(Rset.next( )){;
        int Id = Rset.getInt("ID");
        String sFirstName = Rset.getString("FIRSTNAME");
        String sLastName = Rset.getString("LASTNAME");
        String sTitle = Rset.getString("TITLE");
        String sDepartment = Rset.getString("DEPARTMENT");
        String sEmail = Rset.getString("EMAIL");
        
       
        System.out.println(Id+" "+sFirstName+"   "+sLastName+"    "+sTitle+" "+sDepartment+"  "+sEmail);
       }
    }
   catch ( SQLException err ) {
        System.out.println( err.getMessage( ) );
    }
  }
}