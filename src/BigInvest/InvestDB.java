/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BigInvest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


public class InvestDB {
 
    public static void main(String[] args) {
    try{
        String host = "jdbc:derby://localhost:1527/BigInvest";
        String dName = "student";
        String dPass= "student";
        Connection conn = DriverManager.getConnection( host, dName, dPass );
        Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        String SQL = "SELECT * FROM STUDENT.Portfolio";
        ResultSet Rset = state.executeQuery( SQL );
       
        System.out.println("ID"+"   "+"SYMBOL"+"  "+"QUANTITY"+"   "+"PRICE"+"    "+"TOTAL");
        while(Rset.next( )){;
        int Id = Rset.getInt("ID");
        String symbol = Rset.getString("Symbol");
        int quantity = Rset.getInt("Quantity");
        double price = Rset.getDouble("Price");
        
        double total = quantity * price;
        System.out.println(Id+" "+symbol+"   "+quantity+"    "+price+" "+"     "+total+"  ");
       }
    }
   catch ( SQLException err ) {
        System.out.println( err.getMessage( ) );
    }
  }
}